package com.emerio.bootcamp.customerservice;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.test.context.junit4.SpringRunner;

import com.emerio.bootcamp.customerservice.entity.Customer;
import com.emerio.bootcamp.customerservice.repo.CustomerRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.discovery.shared.Application;

import net.minidev.json.JSONArray;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultHandler;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = CustomerServiceApplication.class)
@WebAppConfiguration
public class CustomerServiceApplicationTests {
	   private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
	            MediaType.APPLICATION_JSON.getSubtype(),
	            Charset.forName("utf8"));

	    private MockMvc mockMvc;

	    private String URIcustomer = "/customers";

	    private HttpMessageConverter mappingJackson2HttpMessageConverter;

	    private Customer customer1;
	    private Customer customer2;
	    private Customer customer3;
	    
	    private Timestamp currenttime;
	    private Timestamp currenttime2;
	    
	    @Autowired
	    private CustomerRepository customerRepository;

	    @Autowired
	    private WebApplicationContext webApplicationContext;

	    @Autowired
	    void setConverters(HttpMessageConverter<?>[] converters) {

	        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
	            .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
	            .findAny()
	            .orElse(null);

	        assertNotNull("the JSON message converter must not be null",
	                this.mappingJackson2HttpMessageConverter);
	    }

	    @Before
	    public void setup() throws Exception {
	        this.mockMvc = webAppContextSetup(webApplicationContext).build();

	        this.customerRepository.deleteAll();
	        currenttime = new Timestamp(System.currentTimeMillis());
	        currenttime2 = new Timestamp(System.currentTimeMillis());
	        
	        this.customer1 = new Customer("alorboy");
	        //this.customer1.setUsername("alorboy");
	        this.customer1.setFirstname("AlorAja");
	        this.customer1.setAddress("Emerio City");
	        this.customer1.setDateofbirth(currenttime);
	       
	        this.customer2 = new Customer("alorman");
	        //this.customer2.setUsername("alorman");
	        this.customer2.setFirstname("AlorBanget");
	        this.customer2.setAddress("Emerio Village");
	        this.customer2.setDateofbirth(currenttime2);
	        
	        this.customer3 = new Customer("alorman");
	        //this.customer1.setUsername("alorboy");
//	        this.customer3.getUsername();
	        this.customer3.setFirstname("Alorupdate");
	        this.customer3.setAddress("Emerio City");
//	        this.customer3.setDateofbirth(currenttime2);
	        
	        customerRepository.save(customer2); //this will be save customer 2 before test begin
	    }

	    
	    // Create new account customer 1	    
	    // @Test
	    // public void createUser() throws Exception {
	    //     mockMvc.perform(post(this.URIcustomer)
	    //             .content(this.json(customer1))
	    //             .contentType(contentType))
	    //             .andExpect(status().isOk());
	    // }
	    
	    // Searching existed account customer 2 directly to specific page
	    @Test
	    public void buserFound() throws Exception {
	    	
	        ResultActions ra = mockMvc.perform(get(this.URIcustomer+"/"+this.customer2.getUsername()));
	        	ra.andExpect(content().contentType(contentType));
	        	ra.andExpect(status().isOk());
	        	ra.andExpect(content().json(this.json(customer2)));
	    }	
	    
		  // Searching existed Account Customer 2 by using costumer data list
	    @Test
	    public void busersFound() throws Exception {
	    	 mockMvc.perform(get(this.URIcustomer))
	       .andExpect(status().isOk())
	       .andExpect(jsonPath("$[0].username", is("alorman")));
	    }	
	    
	    // Searching non existed Account 1 directly to specific page
	    @Test
	    public void ausernotFound() throws Exception {
	    	
	        ResultActions ranf = mockMvc.perform(get(this.URIcustomer+"/"+this.customer1.getUsername()));
	        	ranf.andExpect(status().isNotFound());
	    }	
 
	    // Updating Acccount Customer 1 to Customer 3
	    // @Test
	    // public void updateUser() throws Exception {
	    //     mockMvc.perform(put(this.URIcustomer)
	    //             .content(this.json(customer3))
	    //             .contentType(contentType))
	    //             .andExpect(status().isCreated());
	    // }
	    
	   
	    // // Deleting Account Customer 2
	    // @Test
	    // public void deleteAccount() throws Exception {
	    //     ResultActions rada = mockMvc.perform(delete(this.URIcustomer+"/"+this.customer2.getUsername()));
	    //     	rada.andExpect(status().isOk());
	    // }	



    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }
	
}

